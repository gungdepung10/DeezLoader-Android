package com.dt3264.deezloader

import android.Manifest
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.preference.PreferenceManager
import androidx.webkit.WebViewClientCompat
import kotlinx.android.synthetic.main.activity_browser.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import net.lingala.zip4j.ZipFile
import net.lingala.zip4j.progress.ProgressMonitor
import java.io.File
import java.net.URL
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock
import kotlin.system.exitProcess

// Force reloading node data every time the app starts, for debugging only
var forceReloadNodeData = false

// Variable that holds the storage permissions for ease
val storagePerms = arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)

// Constant tag used in logcat
const val tag = BuildConfig.APPLICATION_ID

// Variable to hold the apps file directory path
lateinit var appFilesDir: String

// Build number of the running app
const val clientBuildNumber = BuildConfig.VERSION_CODE

const val telegramURL = "https://t.me/joinchat/Ed1JxEfoci8sv2dVwTUQ3A"
const val serverURL = "http://localhost:1730"
const val serviceChannelID = "${BuildConfig.APPLICATION_ID}.service"
const val downloadChannelID = "${BuildConfig.APPLICATION_ID}.downloads"

// Notification ID for the NodeJS service
const val serviceNotificationID = 10

// Variable to hold the last app version that ran
var lastBuildNumber = 0

// Application-wide shared preferences
lateinit var sharedPreferences: SharedPreferences

val lock = ReentrantLock()
val condition: Condition = lock.newCondition()

external fun startNodeServer(arguments: Array<String>)

class NodeThread : Thread() {
    var started = false

    override fun run() {
        started = true
        System.loadLibrary("native-lib")
        System.loadLibrary("node")
        startNodeServer(arrayOf("node", "$appFilesDir/deezloader/app.js"))
    }
}

val nodeThread = NodeThread()

class MainActivity : AppCompatActivity() {
    private var nodeService: Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        appFilesDir = applicationContext.filesDir.absolutePath
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        lastBuildNumber = sharedPreferences.getInt("lastBuildNumber", 0)

        telegramButton.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(telegramURL)
                }
            )
        }

        openAppExternalButton.setOnClickListener {
            startActivity(
                Intent(Intent.ACTION_VIEW).apply {
                    data = Uri.parse(serverURL)
                }
            )
        }

        openAppInternalButton.setOnClickListener {
            startActivity(
                Intent(applicationContext, BrowserActivity()::class.java).apply {
                    action = Intent.ACTION_MAIN
                    addCategory(Intent.CATEGORY_LAUNCHER)
                    addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                }
            )
        }

        // Check for updates on its own thread, we dont depend on its success or failure to continue
        GlobalScope.launch {
            checkForUpdates()
        }

        // Ensure the device is using a valid architecture for the NodeJS library
        if (!checkDeviceArchitecture()) return

        // Check for storage access, the rest of the app starts when a response is returned (onRequestPermissionsResult)
        ActivityCompat.requestPermissions(this, storagePerms, 100)
    }

    override fun onDestroy() {
        if (nodeService != null) {
            stopService(nodeService)
            nodeService = null
        }

        if (nodeThread.isAlive) {
            nodeThread.interrupt()
        }

        super.onDestroy()
    }

    override fun onBackPressed() {
        // Show a close confirmation popup but only if the landing page is in the foreground
        // Otherwise, the webview will become unresponsive to touch
        if (this@MainActivity.hasWindowFocus()) {
            runOnUiThread {
                AlertDialog.Builder(this@MainActivity).apply {
                    setMessage(R.string.exit_back)
                    setCancelable(true)
                    setPositiveButton(R.string.confirmation) { _, _ ->
                        finish()
                        exitProcess(0)
                    }
                    setNegativeButton(R.string.denial) { dialogInterface, _ ->
                        dialogInterface.cancel()
                    }
                    create().show()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Kickoff the updated apk check and start the service since storage access is granted
                Thread {
                    checkIsApkUpdated()
                    startDeezloaderService()
                }.apply { start() }
            } else {
                // Storage access denied, show a popup for that
                runOnUiThread {
                    progressBar.visibility = View.GONE
                    telegramButton.visibility = View.GONE
                    AlertDialog.Builder(this@MainActivity).apply {
                        setMessage(R.string.permission)
                        setCancelable(false)
                        setPositiveButton(R.string.dismiss_okay) { _, _ ->
                            finish()
                            exitProcess(0)
                        }
                        create().show()
                    }
                }
            }
        }
    }

    private fun checkDeviceArchitecture(): Boolean {
        try {
            if (System.getProperty("os.arch", "arm")!!.contains(Regex("(?i)(arm|aarch)"))) {
                return true
            } else if (System.getProperty("ro.dalvik.vm.native.bridge", "0")!! == "1") {
                return true
            }
        } catch (e: Exception) {
            return true
        }

        runOnUiThread {
            progressBar.visibility = View.GONE
            telegramButton.visibility = View.GONE
            AlertDialog.Builder(this@MainActivity).apply {
                setMessage(R.string.unsupported_architecture)
                setCancelable(false)
                setPositiveButton(R.string.dismiss_okay) { _, _ ->
                    finish()
                    exitProcess(0)
                }
                create().show()
            }
        }

        return false
    }

    private fun checkForUpdates() {
        val updateHolder: String
        val updateArray: Array<String>

        try {
            updateHolder = URL("https://pastebin.com/raw/rEubX2Lu").readText()
        } catch (e: Exception) {
            return
        }

        if (updateHolder.isBlank() or updateHolder.contains("<!DOCTYPE html>")) {
            return
        } else {
            updateArray = updateHolder.split("\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        }

        if (updateArray.isNotEmpty() && updateArray.size >= 3) {
            if (BuildConfig.VERSION_NAME != updateArray[0].trim { it <= ' ' }) {
                val newVersionName = updateArray[0].trim { it <= ' ' }

                val changelogText = updateArray.drop(2).joinToString("\n- ", "\n- ")

                val updateString = "A new version ($newVersionName) is available.\n$changelogText"
                val updateLink = updateArray[1]

                // Show the updater UI elements when a remote version differing from the local is detected
                runOnUiThread {
                    updateText.text = updateString

                    updateButton.setOnClickListener {
                        val updateRequest = DownloadManager.Request(Uri.parse(updateLink)).apply {
                            setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                            setMimeType("application/vnd.android.package-archive")
                        }

                        val downloadManager = applicationContext.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

                        downloadManager.enqueue(updateRequest)
                    }

                    updateText.visibility = View.VISIBLE
                    updateButton.visibility = View.VISIBLE
                }
            }
        }
    }

    // Function to check whether the local assets are up to date, if not update them
    private fun checkIsApkUpdated() {
        // Reload upon one of the following conditions, force reload (debug) or apk update
        if (forceReloadNodeData || lastBuildNumber != clientBuildNumber) {
            runOnUiThread { unzippingProgressBar.visibility = View.VISIBLE }

            //Recursively delete any existing nodejs-project.
            val nodeDirOld = "$appFilesDir/deezerLoader"
            val nodeDir = "$appFilesDir/deezloader"
            val nodeZipExternal = "$appFilesDir/deezloader.zip"

            val nodeZipInternalStream = applicationContext.assets.open("deezloader.zip")

            val nodeDirReferenceOld = File(nodeDirOld)
            val nodeDirReference = File(nodeDir)
            val nodeZipExternalReference = File(nodeZipExternal)

            nodeDirReferenceOld.deleteRecursively()

            for (i in 0..1) {
                try {
                    nodeDirReference.deleteRecursively()
                    nodeZipExternalReference.delete()

                    // Copy the zip file from the APK assets to the apps private files
                    nodeZipInternalStream.copyTo(nodeZipExternalReference.outputStream())

                    // Extract it
                    val deezloaderZip = ZipFile(nodeZipExternal).apply {
                        isRunInThread = true
                    }

                    val progressMonitor = deezloaderZip.progressMonitor
                    deezloaderZip.extractAll(nodeDir)

                    runOnUiThread { unzippingProgressBar.isIndeterminate = false }

                    while (progressMonitor.state != ProgressMonitor.State.READY) {
                        runOnUiThread { unzippingProgressBar.progress = progressMonitor.percentDone }
                        Thread.sleep(100)
                    }

                    runOnUiThread { unzippingProgressBar.visibility = View.GONE }

                    break
                } catch (e: Exception) {
                    Log.e(tag, e.message!!)
                }
            }

            // Update the preference holding the last build number
            sharedPreferences.edit().apply {
                putInt("lastBuildNumber", clientBuildNumber)
                apply()
            }

            // Delete the zip file after extraction
            nodeZipExternalReference.delete()
        }
    }

    private fun startDeezloaderService() {
        if (!File("$appFilesDir/deezloader/app.js").exists()) {
            forceReloadNodeData = true
            checkIsApkUpdated()
            forceReloadNodeData = false
        }

        nodeService = Intent(applicationContext, DeezloaderService()::class.java)
        startService(nodeService)
        lock.withLock { condition.await() }

        // Hide the loading spinner, show the open buttons
        runOnUiThread {
            progressBar.visibility = View.GONE
            openAppExternalButton.visibility = View.VISIBLE
            openAppInternalButton.visibility = View.VISIBLE
        }
    }
}

class BrowserActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {
            setContentView(R.layout.activity_browser)

            internalWebView.apply {
                webViewClient = WebViewClientCompat()
                settings.apply {
                    domStorageEnabled = true
                    javaScriptEnabled = true
                    databaseEnabled = true
                    allowFileAccess = true
                    allowFileAccessFromFileURLs = true
                    allowContentAccess = true
                }
            }.loadUrl(serverURL)
        } catch (e: Exception) {
            Log.e(tag, e.message!!)
        }
    }
}
